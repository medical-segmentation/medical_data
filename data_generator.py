from glob import glob
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model
from keras.utils import to_categorical
import keras.utils
import scipy.misc
import skimage.io
import numpy as np
import random
import os

from .io import load

try:
    from tqdm import tqdm
except:
    print('TQDM does make much nicer wait bars...')
    tqdm = lambda x: x


class DataGenerator(keras.utils.Sequence):
    def __init__(self,
                 samples_path,
                 images_prefix="images",
                 labels_prefix="labels",
                 datagen=ImageDataGenerator(),
                 batch_size=16,
                 shape=(512, 512),
                 num_channels=1,
                 num_classes=1,
                 shuffle=True):
        self.samples_path = samples_path
        self.images_prefix = images_prefix
        self.labels_prefix = labels_prefix
        image_path = sorted(os.listdir(os.path.join(samples_path, images_prefix)))
        label_path = sorted(os.listdir(os.path.join(samples_path, labels_prefix)))
        self.samples = list(zip(image_path, label_path))
        self.datagen = datagen
        self.batch_size = batch_size
        self.shape = shape
        self.num_channels = num_channels
        self.num_classes = num_classes
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        return int(np.floor(len(self.samples) / self.batch_size))

    def __getitem__(self, index):
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]
        batch_samples = [self.samples[k] for k in indexes]
        images = np.empty((self.batch_size, *self.shape, self.num_channels))
        labels = np.empty((self.batch_size, *self.shape, self.num_classes))
        skimage.io.use_plugin("tifffile")
        for i, sample in enumerate(batch_samples):
            image = skimage.io.imread(os.path.join(self.samples_path, self.images_prefix, sample[0]))
            images[i] = np.expand_dims(image, axis=-1)
            label = skimage.io.imread(os.path.join(self.samples_path, self.labels_prefix, sample[1]))
            labels[i] = to_categorical(label, num_classes=self.num_classes)

        self.datagen.fit(images)
        images = self.datagen.standardize(images)

        return images, labels

    def on_epoch_end(self):
        self.indexes = np.arange(len(self.samples))
        if self.shuffle:
            np.random.shuffle(self.indexes)


def load_dataset(samples_path,
                 train_subset="train",
                 test_subset="test",
                 datagen=ImageDataGenerator(),
                 batch_size=16,
                 shape=(512, 512),
                 num_channels=1,
                 num_classes=1):
    train = DataGenerator(samples_path=os.path.join(samples_path, train_subset), datagen=datagen,
                          shape=shape, batch_size=batch_size, num_channels=num_channels, num_classes=num_classes)
    test = DataGenerator(samples_path=os.path.join(samples_path, test_subset), datagen=datagen,
                         shape=shape, batch_size=batch_size, num_channels=num_channels, num_classes=num_classes)

    return train, test


def load_test_batch(samples_path,
                    test_subset="test",
                    datagen=ImageDataGenerator(),
                    batch_size=16,
                    shape=(512, 512),
                    num_channels=1,
                    num_classes=1):
    return DataGenerator(samples_path=os.path.join(samples_path, test_subset), datagen=datagen,
                         shape=shape, batch_size=batch_size, num_channels=num_channels,
                         num_classes=num_classes).__getitem__(0)


def load_test_all(samples_path,
                  test_subset="test",
                  datagen=ImageDataGenerator(),
                  batch_size=16,
                  shape=(512, 512),
                  num_channels=1,
                  num_classes=1):
    return DataGenerator(samples_path=os.path.join(samples_path, test_subset), datagen=datagen,
                         shape=shape, batch_size=batch_size, num_channels=num_channels, num_classes=num_classes,
                         shuffle=False)

# def load_crf_training(task, model,
#                       images_template='imagesTr/la*.nii.gz',
#                       labels_template='labelsTr/la*.nii.gz',
#                       shape=(512, 512),
#                       batch_size=16,
#                       datagen=ImageDataGenerator(),
#                       validation_split=0.2):
#     image_path = sorted(glob(os.path.join(task, images_template)))
#     label_path = sorted(glob(os.path.join(task, labels_template)))
#     samples = list(zip(image_path, label_path))
#     random.shuffle(samples)
#     # test_samples = samples[0:int(validation_split * len(samples))]
#     test_samples = samples[0:1]
#     # train_samples = samples[int(validation_split * len(samples)) + 1:len(samples) - 1]
#     train_samples = samples[1:2]
#
#     test_images = np.empty((0, shape[0], shape[1], 1))
#     test_unaries = np.empty((0, shape[0], shape[1], 1))
#     test_labels = np.empty((0, shape[0], shape[1], 1))
#     for sample_path in tqdm(test_samples):
#         image = load(sample_path[0])
#         label = load(sample_path[1])
#         for z in tqdm(range(label.shape[-1])):
#             img = image[:, :, z]
#             mask = label[:, :, z]
#             if np.count_nonzero(mask) > 0:
#                 img = np.expand_dims(img, axis=-1)
#                 mask = np.expand_dims(mask, axis=-1)
#                 unaries = model.predict(np.expand_dims(img, axis=0))
#                 test_images = np.concatenate((test_images, np.expand_dims(img, axis=0)))
#                 test_unaries = np.concatenate((test_unaries, unaries))
#                 test_labels = np.concatenate((test_labels, np.expand_dims(mask, axis=0)))
#     datagen.fit(test_images)
#
#     train_images = np.empty((0, shape[0], shape[1], 1))
#     train_unaries = np.empty((0, shape[0], shape[1], 1))
#     train_labels = np.empty((0, shape[0], shape[1], 1))
#     for sample_path in tqdm(train_samples):
#         image = load(sample_path[0])
#         label = load(sample_path[1])
#         for z in tqdm(range(label.shape[-1])):
#             img = image[:, :, z]
#             mask = label[:, :, z]
#             if np.count_nonzero(mask) > 0:
#                 img = np.expand_dims(img, axis=-1)
#                 mask = np.expand_dims(mask, axis=-1)
#                 unaries = model.predict(np.expand_dims(img, axis=0))
#                 train_images = np.concatenate((train_images, np.expand_dims(img, axis=0)))
#                 train_unaries = np.concatenate((train_unaries, unaries))
#                 train_labels = np.concatenate((train_labels, np.expand_dims(mask, axis=0)))
#
#     identity = ImageDataGenerator()
#     train_generator = datagen.flow(train_images, train_labels, batch_size=batch_size)
#     train_unaries_generator = identity.flow(train_unaries, batch_size=batch_size)
#     test_generator = datagen.flow(test_images, test_labels, batch_size=batch_size)
#     test_unaries_generator = identity.flow(test_unaries, batch_size=batch_size)
#
#     def data_generator(generator, unaries_generator):
#         while True:
#             data = next(generator)
#             yield ([next(unaries_generator), data[0]], data[1])
#
#     return data_generator(train_generator, train_unaries_generator), \
#            data_generator(test_generator, test_unaries_generator)

# def crf_test_batch(task,
#                    model,
#                    images_template='imagesTr/la*.nii.gz',
#                    labels_template='labelsTr/la*.nii.gz',
#                    shape=(512, 512),
#                    batch_size=16,
#                    datagen=ImageDataGenerator(validation_split=0.2)):
#     batch = load_test_batch(task, images_template, labels_template, shape, batch_size, datagen)
#     return [batch[0], model.predict(batch[0])], batch[1]
