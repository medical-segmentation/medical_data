import numpy as np
import os
import gzip

def load_ids(filename, shape, zipped=False, normalization=True):
    if zipped:
        with gzip.open(filename, "r") as f:
            content = f.read()
    else:
        with open(filename, "rb") as f: 
            content = f.read()
    data = np.moveaxis(np.reshape(np.frombuffer(content, dtype=np.uint16), shape), 0, 2)
    if normalization:
        res = data / np.max(np.max(np.max(data)))
    else:
        res = data
    return res

def parse_ics(content):
    res = dict()
    # Split each line to array and remove empty lines
    lines_data = list(filter(lambda x: len(x) > 0, map(lambda line: line.split(), content.split("\n"))))
    try:
        def extract_data(condition):
            return list(filter(condition, lines_data)).pop()

        res["filename"] = extract_data(lambda line: line[0] == "filename")[1]
        layout_sizes = extract_data(lambda line: len(line) > 1 and line[1] == "sizes")
        res["shape"] = tuple(reversed(list(map(int, layout_sizes[3:]))))
        try:
            compression = extract_data(lambda line: line[0] == "representation" and line[1] == "compression")
            res["compression"] = compression[2]
        except IndexError:
            pass
        return res
    except IndexError:
        raise Exception("Can't find the sizes property value in ICS")

def load_ics(filename, normalization=True):
    with open(filename, "r") as f:
        properties = parse_ics(f.read())
        # ids_filename = filename.lower().rstrip(".ics") + ".ids"
        # Replace ics filename with ids filename preserving file path
        # ids_filename = "/".join(filename.split("/")[:-1] + [properties["filename"]])
        ids_filename = os.path.join(os.path.dirname(filename), properties["filename"])
        zipped = False
        if "compression" in properties:
            zipped = True
        return load_ids(ids_filename, properties["shape"], zipped=zipped, normalization=normalization)
    raise FileNotFoundError("Wrong filename")