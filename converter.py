from .io import load, save

def convert(input_filename, output_filename, ext=None):
    data = load(input_filename)
    if ext is not None:
        save(output_filename, data)
    else:
        save(output_filename, data, ext=ext)
        