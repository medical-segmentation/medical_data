import warnings
# Suppressing FutureWarning of hdf5
warnings.simplefilter(action='ignore', category=FutureWarning)

import nibabel as nib
from .loaders.ics_loader import load_ics
from .savers.ics_saver import save_ics

import numpy as np
from abc import ABC, abstractmethod
import os

class MedicalIO(ABC):
    @abstractmethod
    def load(self, filename, *args, **kwargs):
        pass
    @abstractmethod
    def save(self, filename, data, *args, **kwargs):
        pass
    
    
class ICS_IO(MedicalIO):
    def load(self, fname):
        return load_ics(fname, normalization=True)
    def save(self, fname, data):
        save_ics(fname, data)
    
class NIFTI_IO(MedicalIO):
    def load(self, fname):
        nib_object = nib.load(fname)
        return nib_object.get_fdata()
    def save(self, fname, data):
        affine_transformation = np.eye(4)
        img = nib.Nifti1Image(data, affine_transformation)
        img.to_filename(fname)
        

# Mapping the file extension to MedicalIO handler
_IO = {
    "ics": ICS_IO,
    "gz": NIFTI_IO,
    "nii": NIFTI_IO
}    
    
_io_caching_enabled = True
_io_cache = dict()
    
def get_IO(ext):    
    if ext not in _IO:
        raise ValueError("There is no MedicalIO for extension: '%s'" % ext)
    if _io_caching_enabled:
        if ext not in _io_cache:
            _io_cache[ext] = _IO[ext]()
        return _io_cache[ext]
    else:
        return _IO[ext]()

def extract_extension(filename, kwargs):
    path, ext = os.path.splitext(filename)
    extension = None
    if "ext" in kwargs:
        extension = kwargs.pop("ext")
    if extension is None:
        extension = ext.lstrip(".")
    return extension
    
def load(filename, *args, **kwargs):
    loader = get_IO(extract_extension(filename, kwargs))
    return loader.load(filename, *args, **kwargs)

def save(filename, data, *args, **kwargs):
    exporter = get_IO(extract_extension(filename, kwargs))
    return exporter.save(filename, data, *args, **kwargs)