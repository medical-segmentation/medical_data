import numpy as np
import os
import gzip

def save_ids(filename, data, zipped=False):
    converted = np.moveaxis(data, 2, 0).astype(dtype=np.uint16)
    if not zipped:
        converted.tofile(filename)
    else:
        with gzip.open(filename, "w") as f:
            converted.tofile(f)

def print_inf(filename, properties):
    template = """VERSION 1
GREY_LOGICAL_DEPTH 16
GEO_ORIGIN -3.148658398593696 -13.281530023913689 22.329359111444006
GEO_AXIS_X 0.060784165115214797 0.99810398116063603 0.0096813254534647807
GEO_AXIS_Y 0.044122117510732202 0.00700301138255201 -0.99900159988757997
GEO_AXIS_Z -0.99717527246598703 0.061150638777247801 -0.043612788944518401
SIZE {shape[0]} {shape[1]} {shape[2]}
GEO_EXTENT 256 256 167.9998804634304
VOXEL_GAP 0 0 0"""
    with open(filename, "w") as f:
        f.write(template.format(**properties))
    
def print_ics(filename, properties):
    template = """	
ics_version	1.0
filename	{filename}
layout	parameters	4
layout	order	bits	x	y	z
layout	sizes	16	{shape[0]}	{shape[1]}	{shape[2]}
layout	coordinates	video
layout	significant_bits	16
representation	byte_order	1	2
representation	format	integer
representation	sign	unsigned
representation	SCIL_TYPE	g3d
parameter	scale	1	1	1	1.19999914616736
parameter	origin	0	66.640149116516099	-146.53263121843005	152.10499811172394
parameter	axisX	0	0.060784165115214797	0.99810398116063603	0.0096813254534647807
parameter	axisY	0	0.044122117510732202	0.00700301138255201	-0.99900159988757997
parameter	axisZ	0	-0.99717527246598703	0.061150638777247801	-0.043612788944518401
"""
    if "compression" in properties:
        compression = "\nrepresentation	compression	gzip"
        template = template + compression
    with open(filename, "w") as f:
        f.write(template.format(**properties))

def save_ics(filename, data, zipped=False):
    ics_basename = os.path.basename(filename)
    ics_without_extension = ics_basename.rstrip(".ics")

    ids_basename = ics_without_extension + ".ids"
    ids_filename = os.path.join(os.path.dirname(filename), ids_basename)
    save_ids(ids_filename, data, zipped)

    properties = dict()
    properties["filename"] = ids_basename
    properties["shape"] = data.shape
    if zipped:
        properties["compression"] = "gzip"

    inf_basename = ics_without_extension + ".inf"
    inf_filename = os.path.join(os.path.dirname(filename), inf_basename)
    print_inf(inf_filename, properties)
        
    print_ics(filename, properties)
        