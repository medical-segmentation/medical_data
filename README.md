﻿# medical_data
A python package for handling the medical data.
Modules:
* io - save and load medical images
* metrics - compare two segmentation masks
* converter - convert medical image from one format to another

## Getting Started
Clone repository into working directory to use package locally. Your working directory will look like this:

> medical_data/
>
> your_python_files.py
>
> ...
### Loading and saving medical data

    from medical_data.io import load, save
    data = load("input.ics")
    ... process somehow ...
    save("output.nii.gz", data)
To specify the file extension use the `ext` named argument:

    data = load("input.data", ext="ics") 
    save("output.data", ext="ics")

To prepare data for training keras model use data_generator extension:
    
    from medical_data.data_generator import load_dataset
    # Keras preprocessing tool. For more information look up official documentation.
    datagen = ImageDataGenerator(
        featurewise_center=True,
    	featurewise_std_normalization=True,
    	validation_split=0.2
    )
    # Method requies path to dataset folder, batch parameters and keras data generator. Method returns two generators of train and validation data which are meant to be used with keras fit_generator
    train, test = load_dataset("D:\MachineLearning\DecathlonDataset\Task02_Heart",
                               shape=(320, 320), batch_size=4, datagen=datagen)

### Calculating metrics

    from medical_data.metrics import compare
    my_mask = ...
    golden_mask = ...
    metrics = compare(my_mask, golden_mask)
   The `compare` function has no multi-class support now.

## Built With
* NiBabel
* SciKit-Learn
