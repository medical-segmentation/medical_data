import numpy as np
import sklearn.metrics as metrics

def compare_flat(mask_flatten, valid_flatten):
    """Calculate recall, precision, accuracy, dice, roc_auc"""
    confusion_matrix = metrics.confusion_matrix(valid_flatten, mask_flatten)
    TP = confusion_matrix[1, 1] # true positive
    FP = confusion_matrix[0, 1] # false positive
    FN = confusion_matrix[1, 0] # false negative
    TN = confusion_matrix[0, 0] # true negative
    recall = TP / (TP + FN)
    precision = TP / (TP + FP)
    accuracy = (TP + TN) / (TP + TN + FP + FN)
    dice = (2. * TP) / (2. * TP + FP + FN)
    roc_auc = metrics.roc_auc_score(valid_flatten, mask_flatten)
    return {
        "TP": TP,
        "FP": FP,
        "FN": FN,
        "TN": TN,
        "recall": recall,
        "precision": precision,
        "accuracy": accuracy,
        "dice": dice,
        "roc_auc": roc_auc
    }

def compare(mask, valid):
    mask_len = np.prod(mask.shape)
    mask_flatten = np.reshape(mask, mask_len)
    valid_flatten = np.reshape(valid, mask_len) # not a typo
    return compare_flat(mask_flatten, valid_flatten)